.. _part2:

*************************************************************************************************
Partie 2 | Parsing
*************************************************************************************************

Question 1 :
------------

Give the left recursion removal algorithm. Use this algorithm to remove recursion in the following grammar :

1. S := Aa | b
2. A := Bc | e
3. B := Se | d

Question 2 :
------------

Write an unambiguous grammar for arithmetic expression.

Question Title proposed by Group 43, Minet Jeremy and Charlier Gilles
=====================================================================

Grammar :
---------

1. E ::= TQ
2. Q ::= +TQ
3. Q ::= -TQ
4. Q ::= ε
5. T ::= FR
6. R ::= \*FR
7. R ::= /FR
8. R ::= ε
9. F ::= (E)
10. F ::= id

Steps of derivation :
---------------------

+-----------------------------------+-----------------------------------+-----------------------------------+
| Stack                             | Input                             | Output                            |
+===================================+===================================+===================================+
| #E                                | *id*/(id-id)#                     |                                   |
+-----------------------------------+-----------------------------------+-----------------------------------+
| #QT                               | *id*/(id-id)#                     | 1                                 |
+-----------------------------------+-----------------------------------+-----------------------------------+
| \.\.\.                            | \.\.\.                            | \.\.\.                            |
+-----------------------------------+-----------------------------------+-----------------------------------+

Questions :
-----------

Given the above grammar, answer the following questions :

1. What should be the result of **table[R,)]** ?
2. How many entries of the corresponding table should be filled ?
3. How many times is the 8th rule applied ?
4. Compute the first set of T.
5. Given the input **id/(id - id)**, mimic the behaviour of the LL(1) parser by completing the above top-down derivation.

Hint :
------

As you may have figured out, building the corresponding table of this grammar will save you a lot of time.



CFG Disambiguation proposed by Group 14, Beznik Thomas and Delaunoy Valentin
==========================================================

Given the following grammar:

* S -> AA
* A -> SS | a

1. Show that this grammar is ambiguous
""""""""""""""""""""""""""""""""""""""

2. Explain the language described by the grammar
""""""""""""""""""""""""""""""""""""""""""""""""

3. Disambiguate the grammar
"""""""""""""""""""""""""""

Answers
"""""""

1.
""

Diagram 1:
	.. image:: img/14-diagram1.png

2.
""
	.. math::
		a^(2+3n)
3.
""

* S -> aaA
* A -> aaaA | epsilon


'CFG' proposed by Group 11, Antoine Denauw & Antonio De Carvalho
----------------------------------------------------------------

Question 1 :
============

What is a Context Free Grammars (CFG) and of what is it compose ?

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Answer:


A CFG is a tuple :

.. math::  G = (N, T, S, P)

* N is a set of non terminal symbols.

* T is a set of terminal symbols (token of language).

* S \e N is the start symbol.

* P is a set of production rules :

.. math:: X ::= Y_1 ... Y_n

.. math:: X ϵ N, Y_i ϵ N ∪ T ∪ {ε}
            


Question 2 :
============

Give a left-most derivation parse tree for :

 id / id * id

For the following set of production rules,

* E ::= E / T
                
* E ::= T
                
* T ::= T * F
                
* T ::= F
                
* F ::= (E)
                
* F ::= id

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Answer:

.. image:: img/Q2_PR_L&T.png

Question 3 :
============

Is this grammar ambiguous ? If yes, correct it.

* E ::= E * E
                
* E ::= E + E

* E ::= (E)

* E ::= id

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Answer:

Yes, it is ambiguous, here is the correction :

* E ::= E * T

* E ::= T

* T ::= T + F

* T ::= F

* F ::= (E)

* F ::= id

Question 4 :
============

Following this grammar:

* E ::= SF

* S ::= (F)S

* S ::= ε

* F ::= id

* F ::= (E)

Which of the following is a correct derivation?

1. E::= (id(id))

2. E::= ((id))((id))()

3. E::= ε

4. E::= (((id)(id)id))


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Answer:

E::= (((id)(id)id))


